
.. _changelog-0.0.7:

0.0.7 — 2024-04-07
------------------

Added
^^^^^

- Endpoint to link a play onto a game

Changed
^^^^^^^

- Expanded FastAPI models, set some fields nullable
- Versioning for Docker containers

.. _changelog-0.0.6:

0.0.6 — 2024-04-06
------------------

Added
^^^^^

- Routes to add, edit and delete games

- Routes to add, edit and delete play sessions

Changed
^^^^^^^

- Full database and FastAPI models with relationships
